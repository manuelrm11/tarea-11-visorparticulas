using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorPS : MonoBehaviour
{

    public ParticleSystem tornado;
    public ParticleSystem colorInterno;
    public ParticleSystem colorRayos;
    public ParticleSystem colorSueloPequeño;
    public ParticleSystem colorSueloGrande;

    public ParticleSystem colorfuegos1;
    public ParticleSystem colorfuegos2;
    public ParticleSystem tamañofuegos1;
    public ParticleSystem tamañofuegos2;

    public ParticleSystem colorFuego;
    public ParticleSystem tamañofuego;

    // Start is called before the first frame update
    void Start()
    {
        

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    

    public void CambiarColor()
    {
        var mainModule = tornado.main;
        mainModule.startColor = Color.gray;
        
    }

    public void ColorInternoTornado()
    {
        var mainModule = colorInterno.main;
        mainModule.startColor = Color.gray;
    }

    public void ColorRayos()
    {
        var mainModule = colorRayos.main;
        mainModule.startColor = Color.red;
    }

    public void ColorSueloPequeño()
    {
        var mainModule = colorSueloPequeño.main;
        mainModule.startColor = Color.red;
    }

    public void ColorSueloGrande()
    {
        var mainModule = colorSueloGrande.main;
        mainModule.startColor = Color.red;
    }

    public void CambioColorArtificiales()
    {
        var mainModule = colorfuegos1.main;
        mainModule.startColor = Color.green;
    }

    public void CambioColorArtificiales2()
    {
        var mainModule = colorfuegos2.main;
        mainModule.startColor = Color.yellow;
    }

    public void CambiarTamañoFuegos1(int Tamaño)
    {
        var mainModule = tamañofuegos1.main;
        mainModule.startSize = mainModule.startSize.constant + Tamaño;
    }

    public void CambiarTamañoFuegos2(int Tamaño)
    {
        var mainModule = tamañofuegos2.main;
        mainModule.startSize = mainModule.startSize.constant + Tamaño;
    }

    public void CambioColorFuego()
    {
        var mainModule = colorFuego.main;
        mainModule.startColor = Color.cyan;
    }

    public void CambiarTamañoFuego(int Tamaño)
    {
        var mainModule = tamañofuego.main;
        mainModule.startSize = mainModule.startSize.constant + Tamaño;
    }

}
